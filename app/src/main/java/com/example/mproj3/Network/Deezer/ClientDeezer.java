package com.example.mproj3.Network.Deezer;

import com.example.mproj3.Constants;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClientDeezer {

    public static Retrofit retrofit= null;

    public static Retrofit clientInstance(){

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();

        if (retrofit==null){
            return new Retrofit.Builder().baseUrl(Constants.BASE_URL_DEEZER)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        else {
            return retrofit;
        }
    }
}
