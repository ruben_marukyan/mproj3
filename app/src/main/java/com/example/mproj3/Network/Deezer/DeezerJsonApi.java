package com.example.mproj3.Network.Deezer;

import com.example.mproj3.Constants;
import com.example.mproj3.ModelsDeezer.DeezerSearchResponse;
import com.example.mproj3.ModelsDeezer.Track.DeezerTrack;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DeezerJsonApi {


    @GET("search")
    Call<DeezerSearchResponse> getSearchedTracks(@Query("q") String searchText,
                                                 @Header(Constants.HEADER) String key);
    @GET("search")
    Call<DeezerSearchResponse> getSearchedTracksNext(@Query("q") String searchText,
                                                 @Query("index") Integer index,
                                                 @Header(Constants.HEADER) String key);

    @GET("track/{id}")
    Call<DeezerTrack> getTrackById(@Path("id") Integer trackId,
                                   @Header(Constants.HEADER) String key);
}
