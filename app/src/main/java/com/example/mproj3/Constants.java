package com.example.mproj3;

public class Constants {
    public static final String BASE_URL = "https://genius.p.rapidapi.com";
    public static final String BASE_URL_DEEZER = "https://deezerdevs-deezer.p.rapidapi.com/";
    public static final String HEADER = "x-rapidapi-key";
    public static final String KEY_1 = "bfb53c3347msh3673386a203fc91p1048c7jsn525b7229575f";
    public static final String KEY_2 = "8a7f56a9bbmsh8da7f3a5250db27p13a8ffjsn3ec711a9bb3e";

    public static final String LOGGED_CHECK = "LoggedCheck";
    public static final String LOGGED_USER_ID = "LoggedUserID";

    public static final String USERS_DOCUMENT_COLLECTION = "users";
    public static final String TRACK_ID = "songId";
    public static final String USERS_FAVORITES_COLLECTION = "favorites.";
    public static final String FAVORITES = "favorites";

    public static final String USER_MAIL = "mail";
    public static final String USER_USERNAME = "username";
    public static final String USER_PHONE_NUMBER = "phoneNumber";
    public static final String USER_BIRTHDAY = "birthday";
}
