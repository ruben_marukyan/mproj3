package com.example.mproj3.Database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "favorites_table"
//        , indices = @Index(value = {"id"}, unique = true)
)
public class FavoriteTrackModel {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "user_id")
    private String userId;

    @ColumnInfo(name = "artist_name")
    private String artistName;

    @ColumnInfo(name = "track_name")
    private String trackName;

    @ColumnInfo(name = "track_id")
    private int trackId;

    public FavoriteTrackModel(String userId, String artistName, String trackName, int trackId) {
        this.userId = userId;
        this.artistName = artistName;
        this.trackName = trackName;
        this.trackId = trackId;
    }

    @Ignore
    public FavoriteTrackModel(int id, String userId, String artistName, String trackName, int trackId) {
        this.id = id;
        this.userId = userId;
        this.artistName = artistName;
        this.trackName = trackName;
        this.trackId = trackId;
    }

    public int getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getTrackName() {
        return trackName;
    }

    public int getTrackId() {
        return trackId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }
}
