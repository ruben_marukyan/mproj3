package com.example.mproj3.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = FavoriteTrackModel.class, version = 1, exportSchema = false)
public abstract class FavoriteTracksRoomDB extends RoomDatabase {

    public abstract FavoriteTracksDao favoriteTracksDao();

    public static volatile FavoriteTracksRoomDB dbInstance;

    public static FavoriteTracksRoomDB getInstance(final Context context){
        if (dbInstance == null){
            synchronized (FavoriteTracksRoomDB.class){
                if (dbInstance == null){
                    dbInstance = Room.databaseBuilder(context.getApplicationContext(),
                            FavoriteTracksRoomDB.class,
                            "favorite_tracks_db")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return dbInstance;
    }
}