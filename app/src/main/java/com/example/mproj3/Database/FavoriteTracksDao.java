package com.example.mproj3.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface FavoriteTracksDao {

    @Insert
    void insertTrack(FavoriteTrackModel track);

    @Delete
    void deleteTrack(FavoriteTrackModel track);

    @Query("SELECT * FROM favorites_table WHERE user_id = :userId")
    List<FavoriteTrackModel> getFavoriteTracks(String userId);

    @Query("SELECT * FROM favorites_table Where user_id = :userId AND track_id = :trackId")
    int exists(String userId,
               int trackId);
//
//    @Query("SELECT * FROM favorites_table WHERE user_id = :userId")
//    public LiveData<List<FavoriteTrackModel>> getFavoriteTracks(String userId);
}
