package com.example.mproj3.Database;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class FavoriteTrackRepository {
    private FavoriteTracksDao favoriteTracksDao;
    private LiveData<List<FavoriteTrackModel>> favoriteTracks;

    public FavoriteTrackRepository(Application application, String userId) {
        FavoriteTracksRoomDB db = FavoriteTracksRoomDB.getInstance(application);
        favoriteTracksDao = db.favoriteTracksDao();
//        favoriteTracks = favoriteTracksDao.getFavoriteTracks(userId);
    }

    LiveData<List<FavoriteTrackModel>> getFavoriteTracks(){
        return favoriteTracks;
    }



//    /*ROOM Database*/
//    private DataDao dao;
//
//    public Repository(DataDao dao) {
//        this.dao = dao;
//    }
//
//    public void addDataToRoom(DataModelRoom dataModelRoom) {
//        dao.addData(dataModelRoom);
//    }
//
//    public void updateDataInRoom(DataModelRoom dataModelRoom) {
//        dao.updateData(dataModelRoom);
//    }
//
//    public void deleteDataFromRoom(String json) {
//        dao.deleteData(json);
//    }
//
//    public void deleteAll() {
//        dao.deleteAll();
//    }
//
//    public LiveData<List<DataModelRoom>> readDataFromRoom() {
//        return dao.readData();
//
//    }
//    public int checkExistence(String json){
//        return dao.checkExistence(json);
//    }

}
