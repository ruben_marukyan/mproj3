package com.example.mproj3.ViewModels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mproj3.Database.FavoriteTrackModel;

import java.util.List;

public class FavoritesFragmentViewModel extends ViewModel {
    MutableLiveData<List<FavoriteTrackModel>> favoriteTracks;

    public  MutableLiveData<List<FavoriteTrackModel>> getFavoriteTracks(){
        if (favoriteTracks == null){
            favoriteTracks = new MutableLiveData<>();
        }
        return favoriteTracks;
    }

    public void setFavoriteTracks(List<FavoriteTrackModel> deezerTracks){
        if (deezerTracks!= null){
            getFavoriteTracks().setValue(deezerTracks);
        }
    }
    
}
