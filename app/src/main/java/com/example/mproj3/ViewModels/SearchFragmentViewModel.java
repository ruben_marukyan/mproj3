package com.example.mproj3.ViewModels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mproj3.ModelsDeezer.Track.DeezerTrack;

import java.util.List;

public class SearchFragmentViewModel extends ViewModel {

    MutableLiveData<List<DeezerTrack>> tracks;

    public MutableLiveData<List<DeezerTrack>> getTracks(){
        if (tracks == null){
            tracks = new MutableLiveData<>();
        }
        return tracks;
    }

    public void setTracks(List<DeezerTrack> deezerTracks){
        if(deezerTracks!=null){
            getTracks().setValue(deezerTracks);
        }
    }
}
