package com.example.mproj3.Dialogues;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;
import java.util.Objects;

public class MyDatePickerDialog extends DatePickerDialog {

    public MyDatePickerDialog(@NonNull Context context, @Nullable OnDateSetListener listener, int year, int month, int dayOfMonth) {
        super(context, listener, year, month, dayOfMonth);
    }

    

//    public interface MyDatePickerInterface{
//        void onDateChanged();
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.N)
//    @NonNull
//    @Override
//    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
//        Calendar calendar = Calendar.getInstance();
//        return new DatePickerDialog(requireActivity(),
//                (DatePickerDialog.OnDateSetListener) getActivity(),
//                calendar.get(Calendar.YEAR),
//                calendar.get(Calendar.MONTH),
//                calendar.get(Calendar.DAY_OF_MONTH));
//    }


}
