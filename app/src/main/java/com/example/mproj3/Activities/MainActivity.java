package com.example.mproj3.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.mproj3.Constants;
import com.example.mproj3.Database.FavoriteTrackModel;
import com.example.mproj3.Fragments.FragmentFavorites;
import com.example.mproj3.Fragments.FragmentProfile;
import com.example.mproj3.Fragments.FragmentSearch;
import com.example.mproj3.Fragments.FragmentTrack;

import com.example.mproj3.ModelsDeezer.Track.DeezerTrack;
import com.example.mproj3.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.Map;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemSelectedListener,
        FragmentSearch.FragmentSearchListener,
        FragmentTrack.FragmentSingleSongListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private BottomNavigationView bottomNavigationView;
    private ActionBarDrawerToggle toggle;
    private AppCompatTextView headerUsername;
    private AppCompatTextView headerMail;

    private String userId;
    private String mail;
    private String username;

    private FragmentSearch fragmentSearch;

    public static final String CHANNEL_ID = "personal notification";
    public static final int NOTIFICATION_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        updateUi(intent);
        openSearchFragment();

    }

    private void openSearchFragment() {
        fragmentSearch = new FragmentSearch();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragmentSearch, null)
                .addToBackStack("ARM")
                .commit();
    }

    private void updateUi(Intent intent) {
        if (intent != null && intent.getStringExtra(Constants.LOGGED_USER_ID) != null) {
            setAll();
            userId = intent.getStringExtra(Constants.LOGGED_USER_ID);
            FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
            DocumentReference documentReference = firebaseFirestore
                    .collection(Constants.USERS_DOCUMENT_COLLECTION)
                    .document(userId);
            documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                    if (value != null) {
                        Map<String, Object> document = value.getData();

                        mail = Objects.requireNonNull(document.get(Constants.USER_MAIL)).toString();
                        username = Objects.requireNonNull(document.get(Constants.USER_USERNAME)).toString();
                        showNotification(username);

                        headerMail.setText(mail);
                        headerUsername.setText(username);
                    }
                }
            });
        }
    }

    private void setAll() {
        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.my_drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!toggle.isDrawerIndicatorEnabled()) {
                    onBackClicked();
                }
            }
        });
        bottomNavigationView = findViewById(R.id.my_bottom_navigation_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        navigationView = findViewById(R.id.my_navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        headerMail = headerView.findViewById(R.id.header_mail);
        headerUsername = headerView.findViewById(R.id.header_username);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.item_profile:
                bottomNavigationView.getMenu().getItem(2).setChecked(true);
                openProfileFragment();
                onBackPressed();
                break;
            case R.id.item_explore:
                openSearchFragment();
                break;
            case R.id.item_favorites:
                openFavoritesFragment();
                break;
            case R.id.item_profile_bottom:
                openProfileFragment();
                break;
            case R.id.item_about:
                openDialogFragment();
                break;
            case R.id.item_contact:

                Intent intent=new Intent(Intent.ACTION_SEND);
                String[] recipients={"marukyan.ruben@gmail.com"};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT,"Questions about the DeezerTeaser app");
                intent.putExtra(Intent.EXTRA_TEXT,"Dear creator of the app.\n");
                intent.setType("text/html");
                intent.setPackage("com.google.android.gm");
                startActivity(Intent.createChooser(intent, "Send mail"));

                break;
            case R.id.item_settings:
                Intent notificationIntent = new Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);
                notificationIntent.putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName());
                notificationIntent.putExtra(Settings.EXTRA_CHANNEL_ID, CHANNEL_ID);
                startActivity(notificationIntent);
                break;
            case R.id.item_logOut:
                askToLogOut();
                break;
        }

        return true;
    }

    private void openProfileFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new FragmentProfile(), null).commit();
    }

    private void openDialogFragment() {

        LayoutInflater layoutInflater = getLayoutInflater();
        //ToDo change Layout with Text about app
        View view = layoutInflater.inflate(R.layout.layout_about_app, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("About our app.\n")
                .setView(view)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private void openFavoritesFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new FragmentFavorites()).commit();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.close();
        } else if(!toggle.isDrawerIndicatorEnabled()) {
            onBackClicked();
        } else {
                askToLogOut();
        }
    }

    private void askToLogOut() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Log out")
                .setMessage("Log out from account?")
                .setIcon(R.drawable.ic_error)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences sharedPreferences = getSharedPreferences(Constants.LOGGED_CHECK, MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(Constants.LOGGED_USER_ID, null);
                        editor.apply();
                        finish();
                    }
                })
                .show();
    }

    @Override
    public void onSearchedTrackClicked(int songId) {
        Fragment fragment = new FragmentTrack();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.TRACK_ID, songId);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack("ARM")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        showBackButton(true);

    }

    @Override
    public void onClickedForFavorites(DeezerTrack deezerTrack) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        String userId = auth.getUid();
        DatabaseReference reference = db.getReference(Constants.FAVORITES);

        FavoriteTrackModel favoriteTrackModel =
                new FavoriteTrackModel(
                        userId,
                        deezerTrack.getTitle(),
                        deezerTrack.getArtist().getName(),
                        deezerTrack.getId());

        assert userId != null;

        reference.child(userId).child(String.valueOf(deezerTrack.getId())).setValue(favoriteTrackModel);
        reference.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if (snapshot.hasChild(String.valueOf(deezerTrack.getId()))){
                    Toast.makeText(MainActivity.this,"Song already added to Favourites", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        reference.child(userId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void showBackButton(boolean isBack){
        toggle.setDrawerIndicatorEnabled(!isBack);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isBack);
        toggle.syncState();

    }

    @Override
    public void onBackClicked() {
        super.onBackPressed();
        showBackButton(false);

    }

    private void showNotification(String username) {
        showNotificationSDK_O();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setContentTitle("Login Information");
        builder.setContentText("User " + username + " logged in successfully.");
        builder.setSmallIcon(R.drawable.ic_profile);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());
    }

    private void showNotificationSDK_O() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            CharSequence name = "Login Information";
            String description = "description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationChannel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

}