package com.example.mproj3.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.mproj3.Constants;
import com.example.mproj3.Fragments.LogInFragment;
import com.example.mproj3.Fragments.SignUpFragment;

import com.example.mproj3.Models.UserModel;

import com.example.mproj3.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LoggingActivity extends AppCompatActivity
        implements LogInFragment.LogInFragmentListener,
        SignUpFragment.SignUpFragmentListener {

    private SharedPreferences sharedPref;
    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firebaseFirestore;

    private String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logging);
        checkUserLoggedIn();
        setUpFirebase();
    }

    private void checkUserLoggedIn() {
        String userId;
        sharedPref = getSharedPreferences(Constants.LOGGED_CHECK, MODE_PRIVATE);
        userId = sharedPref.getString(Constants.LOGGED_USER_ID, null);
        if (userId != null) {
            logIn(userId);
        }
    }

    private void addUserInSharedPreferences(String userId) {
        sharedPref = getSharedPreferences(Constants.LOGGED_CHECK, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constants.LOGGED_USER_ID, userId);
        editor.apply();
    }


    private void logIn(String userId) {

        Intent intent = new Intent(LoggingActivity.this, MainActivity.class);
        intent.putExtra(Constants.LOGGED_USER_ID, userId);
        startActivity(intent);
    }

    private void setUpFirebase() {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
    }

    @Override
    public void onLogInClicked(String inputMail, String inputPassword, boolean checkboxChecked) {

        firebaseAuth.signInWithEmailAndPassword(inputMail, inputPassword)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            String userFromFirebase = firebaseAuth.getCurrentUser().getUid();
                            logIn(userFromFirebase);
                            if (checkboxChecked) {
                                addUserInSharedPreferences(userFromFirebase);
                            }
                        }
                        else Toast.makeText(LoggingActivity.this, "Wrong email or password.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void signUpWithNewUser(UserModel user, String password) {
        addFirebaseUser(user, password);
    }


    private void addFirebaseUser(UserModel user, String password) {
        firebaseAuth.createUserWithEmailAndPassword(user.getMail(), password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(LoggingActivity.this, "Success", Toast.LENGTH_SHORT).show();
                            userId = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
                            DocumentReference documentReference = firebaseFirestore
                                    .collection(Constants.USERS_DOCUMENT_COLLECTION)
                                    .document(userId);
                            Map<String, String> userFirebase = new HashMap<>();
                            userFirebase.put(Constants.USER_MAIL, user.getMail());
                            userFirebase.put(Constants.USER_USERNAME, user.getName());
                            userFirebase.put(Constants.USER_PHONE_NUMBER, user.getPhoneNumber());
                            userFirebase.put(Constants.USER_BIRTHDAY, user.getBirthday());
                            documentReference.set(userFirebase).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(LoggingActivity.this, "User added with Info", Toast.LENGTH_SHORT).show();
                                    Log.d("TAG", "onSuccess: user with ID  " + userId);
                                    addUserInSharedPreferences(userId);
                                    logIn(userId);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(LoggingActivity.this, "User added without info", Toast.LENGTH_SHORT).show();
                                    Log.d("TAG", "onFailure: " + e.toString());
                                }
                            });
                        } else {
                            Toast.makeText(LoggingActivity.this, "Fail", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(LoggingActivity.this, "Fail" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("TAG", "onFailure: " + e.getMessage());
                    }
                });
    }

}