package com.example.mproj3.Adapters;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mproj3.ModelsDeezer.Track.DeezerTrack;
import com.example.mproj3.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SearchedSongsAdapter extends RecyclerView.Adapter<SearchedSongsAdapter.SearchedSongViewHolder> {

    private List<DeezerTrack> tracks;
    private SearchedTrackListener listener;

    public SearchedSongsAdapter(List<DeezerTrack> tracks, SearchedTrackListener listener) {
        this.tracks = tracks;
        this.listener = listener;
    }

    public interface SearchedTrackListener {
        void searchedTrackClicked(int songId);
        void onFavoriteClicked(DeezerTrack deezerTrack);
    }

    @NonNull
    @Override
    public SearchedSongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SearchedSongViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_searched_song, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SearchedSongViewHolder holder, int position) {

        holder.hit_title.setText(tracks.get(position).getTitle());
        holder.hit_artist.setText(tracks.get(position).getArtist().getName());
        holder.hit_views_count.setText(tracks.get(position).getAlbum().getTitle());
        Picasso.get().load(tracks.get(position).getAlbum().getCover()).into(holder.hit_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.searchedTrackClicked(tracks.get(position).getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return tracks.size();
    }

    public static class SearchedSongViewHolder extends RecyclerView.ViewHolder

    {

        private AppCompatImageView hit_image;
        private AppCompatTextView hit_title;
        private AppCompatTextView hit_artist;
        private AppCompatTextView hit_views_count;

        public SearchedSongViewHolder(@NonNull View itemView) {
            super(itemView);
            hit_image = itemView.findViewById(R.id.hit_song_image);
            hit_title = itemView.findViewById(R.id.hit_song_title);
            hit_artist = itemView.findViewById(R.id.hit_song_artist);
            hit_views_count = itemView.findViewById(R.id.hit_song_views_count);

        }

    }
}
