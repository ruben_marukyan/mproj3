package com.example.mproj3.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mproj3.Database.FavoriteTrackModel;
import com.example.mproj3.R;

import java.util.List;

public class FavoriteTracksAdapter extends RecyclerView.Adapter<FavoriteTracksAdapter.FavoriteTrackViewHolder> {
    private List<FavoriteTrackModel> tracks;
    private FavoriteTrackListener listener;

    public interface FavoriteTrackListener{
        void removeTrackFromFavorites(FavoriteTrackModel trackModel);
    }

    public FavoriteTracksAdapter(List<FavoriteTrackModel> tracks, FavoriteTrackListener listener) {
        this.tracks = tracks;
        this.listener = listener;
    }

    @NonNull
    @Override
    public FavoriteTrackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FavoriteTrackViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_favorite_track, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteTrackViewHolder holder, int position) {

        holder.favorite_artist.setText(tracks.get(position).getArtistName());
        holder.favorite_track.setText(tracks.get(position).getTrackName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.removeTrackFromFavorites(tracks.get(position));
                removeAt(position);
            }
        });

    }

    private void removeAt(int position) {
        tracks.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return tracks.size();
    }

    public static class FavoriteTrackViewHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView favorite_artist;
        private AppCompatTextView favorite_track;

        public FavoriteTrackViewHolder(@NonNull View itemView) {
            super(itemView);

            favorite_artist = itemView.findViewById(R.id.favorite_artist);
            favorite_track = itemView.findViewById(R.id.favorite_track);
        }
    }
}
