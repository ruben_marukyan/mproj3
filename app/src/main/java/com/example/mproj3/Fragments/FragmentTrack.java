package com.example.mproj3.Fragments;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.example.mproj3.Constants;

import com.example.mproj3.Database.FavoriteTrackModel;
import com.example.mproj3.Database.FavoriteTracksRoomDB;
import com.example.mproj3.ModelsDeezer.Track.DeezerTrack;
import com.example.mproj3.Network.Deezer.ClientDeezer;
import com.example.mproj3.Network.Deezer.DeezerJsonApi;

import com.example.mproj3.R;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentTrack extends Fragment {

    private AppCompatTextView song_title;
    private AppCompatTextView song_artist;
    private AppCompatTextView song_release_date;
    private AppCompatTextView song_page_views;
    private AppCompatImageView song_image;
    private AppCompatButton fav_button;

    private AppCompatImageView playStatusImage;
    private AppCompatTextView textCurrentTime;
    private SeekBar seekBar;

    private MediaPlayer mediaPlayer;
    private Handler handler;

    private DeezerJsonApi deezerJsonApi;
    private FragmentSingleSongListener callback;

    private  DeezerTrack track;

    public interface FragmentSingleSongListener{
        void onBackClicked();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentSingleSongListener){
            callback = (FragmentSingleSongListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_single_song, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        song_title = view.findViewById(R.id.single_song_name);
        song_artist = view.findViewById(R.id.single_song_artist_name);
        song_page_views = view.findViewById(R.id.single_song_pageviews);
        song_release_date = view.findViewById(R.id.single_song_release_date_display);
        song_image = view.findViewById(R.id.single_song_image);
        fav_button = view.findViewById(R.id.button_add_favorites);

        playStatusImage = view.findViewById(R.id.song_play);
        textCurrentTime = view.findViewById(R.id.single_song_play_time);
        seekBar = view.findViewById(R.id.single_song_seekbar);
        handler = new Handler();
        mediaPlayer = new MediaPlayer();

        Bundle bundle = getArguments();
        if (bundle!=null && bundle.getInt(Constants.TRACK_ID,0)!= 0){
            connectNetwork();
            getSong(bundle.getInt(Constants.TRACK_ID));
        }
        listeners();
    }

    private void listeners() {
        seekBar.setMax(100);

        playStatusImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer.isPlaying()){
                    handler.removeCallbacks(updater);
                    mediaPlayer.pause();
                    playStatusImage.setImageResource(R.drawable.ic_play);
                }
                else {
                    mediaPlayer.start();
                    seekBar.setEnabled(false);
                    playStatusImage.setImageResource(R.drawable.ic_pause);
                    updateSeekBar();
                }
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playStatusImage.setImageResource(R.drawable.ic_play);
                seekBar.setProgress(0);
                textCurrentTime.setText(R.string.zero);
            }
        });

        fav_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FavoriteTracksRoomDB favoriteTracksRoomDB = FavoriteTracksRoomDB.getInstance(getActivity());
                String user =  FirebaseAuth.getInstance().getUid();

                FavoriteTrackModel favoriteTrack = new FavoriteTrackModel(
                        user,
                        track.getArtist().getName(),
                        track.getTitle(),
                        track.getId());

                if (favoriteTracksRoomDB.favoriteTracksDao().exists(user, track.getId())==0){
                    favoriteTracksRoomDB.favoriteTracksDao().insertTrack(favoriteTrack);
                }
                else {
                    Toast.makeText(getActivity(), "Track already added.", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void prepareMediaPlayer(){
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(track.getPreview());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mediaPlayer.prepare();
//            Toast.makeText(getActivity(), milliSecondsToTimer(mediaPlayer.getDuration()), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String milliSecondsToTimer(long milliseconds) {
        String timerString = " ";
        String secondsString;

        int hours = (int) (milliseconds/(1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60))/(1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60)/1000);
        if (hours>0){
            timerString = hours + ":";
        }

        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = String.valueOf(seconds);
        }
        timerString = timerString + minutes +":" + secondsString;
        if (timerString.equals("0:30")){
            return "0:00";
        }
        return  timerString;

    }

    private void updateSeekBar(){
        if(mediaPlayer.isPlaying()){
            seekBar.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediaPlayer.getDuration()) * 100));
            handler.postDelayed(updater, 1000);
        }
    }

    private Runnable updater =  new Runnable() {
        @Override
        public void run() {
            updateSeekBar();
            long currentDuration = mediaPlayer.getCurrentPosition();
            textCurrentTime.setText(milliSecondsToTimer(currentDuration));
        }
    };

    private void connectNetwork() {
        deezerJsonApi = ClientDeezer.clientInstance().create(DeezerJsonApi.class);
    }

    private void getSong(int songId) {

        Call<DeezerTrack> call = deezerJsonApi.getTrackById(songId, Constants.KEY_2);
        call.enqueue(new Callback<DeezerTrack>() {
            @Override
            public void onResponse(Call<DeezerTrack> call, Response<DeezerTrack> response) {
                track = response.body();
                song_title.setText(track.getTitle());
                song_artist.setText(track.getArtist().getName());
                song_release_date.setText(track.getReleaseDate());
                song_page_views.setText(String.valueOf(track.getDuration()/60) +" min");
                Picasso.get().load(track.getAlbum().getCover()).into(song_image);

                prepareMediaPlayer();
//                webView.setWebViewClient(new WebViewClient());
//                webView.loadUrl(track.getShare());
            }

            @Override
            public void onFailure(Call<DeezerTrack> call, Throwable t) {
                Toast.makeText(getActivity(), String.valueOf(t.getMessage()), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
