package com.example.mproj3.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.mproj3.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

public class FragmentTEST extends Fragment {

    private FloatingActionButton floatingActionButton;

    private boolean editable = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_test, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        floatingActionButton = view.findViewById(R.id.floating_action_button);
        listeners();
        openProfileFragment();
    }

    private void openProfileFragment() {
        requireActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container_profile, new FragmentProfile()).commit();
    }

    private void openEditProfileFragment() {
        requireActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container_profile, new FragmentProfile()).commit();
    }

    private void listeners() {
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editable){
                    editable = true;
                    floatingActionButton.setImageResource(R.drawable.ic_check);
                    openEditProfileFragment();

                } else {
                    editable = false;
                    floatingActionButton.setImageResource(R.drawable.ic_edit);
                }
            }
        });
    }


}
