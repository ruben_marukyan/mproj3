package com.example.mproj3.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.mproj3.R;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

public class LogInFragment extends Fragment {

    private TextInputLayout usernameOrMail;
    private TextInputLayout password;
    private AppCompatButton button_LogIn;
    private AppCompatCheckBox checkBox;
    private ProgressDialog progressDialog;
    private AppCompatTextView sign_up;
    private AppCompatTextView passwordForgot;

    private String inputMail;
    private String inputPassword;

    private LogInFragmentListener callback;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof LogInFragmentListener){
            callback = (LogInFragmentListener) context;
        }
    }

    public interface LogInFragmentListener{
        void onLogInClicked(String inputMail, String inputPassword, boolean checked);
//        void onSingUpClicked(View view);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_log_in, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        usernameOrMail = view.findViewById(R.id.login_mail);
        password = view.findViewById(R.id.login_password);
        button_LogIn = view.findViewById(R.id.btn_log_in);
        checkBox = view.findViewById(R.id.checkbox_remember);
        progressDialog = new ProgressDialog(getActivity());
        sign_up = view.findViewById(R.id.sign_up_text_view);
        passwordForgot = view.findViewById(R.id.txt_forgot_password_clickable);
        setListeners();
    }

    private void setListeners() {
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_login_fragment_to_sign_up_fragment);
            }
        });
        sign_up.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.action_login_fragment_to_sign_up_fragment));
        passwordForgot.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.action_login_fragment_to_password_fragment));
        button_LogIn.setOnClickListener(v -> {
            inputMail = usernameOrMail.getEditText().getText().toString().trim();
            inputPassword = password.getEditText().getText().toString().trim();
            if (validateLogIn()){
                callback.onLogInClicked(inputMail, inputPassword, checkBox.isChecked());
            }

        });

    }

    private boolean validateLogIn() {
        if (validateEmail() && validatePassword()){
            return true;
        }
        return false;
    }

    private boolean validatePassword() {
        if (Objects.requireNonNull(password.getEditText()).getText().toString().equals("")){
            password.setError("Write your password.");
            return false;
        }else if(password.getEditText().getText().length() < 6){
            password.setError("Password should be at least 6 characters!");
            return false;
        }

        return true;
    }

    private boolean validateEmail() {
        if(!Patterns.EMAIL_ADDRESS.matcher(usernameOrMail.getEditText().getText().toString().trim()).matches()) {
            usernameOrMail.setError("Not a valid Email.");
            return false;
        }
        return true;
    }
}
