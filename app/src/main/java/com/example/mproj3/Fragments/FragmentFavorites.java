package com.example.mproj3.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mproj3.Adapters.FavoriteTracksAdapter;
import com.example.mproj3.Database.FavoriteTrackModel;
import com.example.mproj3.Database.FavoriteTracksDao;
import com.example.mproj3.Database.FavoriteTracksRoomDB;
import com.example.mproj3.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class FragmentFavorites extends Fragment implements FavoriteTracksAdapter.FavoriteTrackListener {

    private AppCompatTextView fav_username;
    private AppCompatTextView fav_mail;
    private RecyclerView fav_recycler;
    private FavoriteTracksDao tracksDao;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favorites, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        fav_username = view.findViewById(R.id.fav_username);
        fav_mail = view.findViewById(R.id.fav_mail);
        fav_recycler = view.findViewById(R.id.fav_list);

        tracksDao = FavoriteTracksRoomDB.getInstance(getActivity()).favoriteTracksDao();

        FirebaseDatabase db = FirebaseDatabase.getInstance();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        String userId = auth.getUid();
        assert userId != null;


        List<FavoriteTrackModel> favoriteTracks = tracksDao.getFavoriteTracks(userId);


        fav_recycler.setAdapter(new FavoriteTracksAdapter(favoriteTracks,this));
        fav_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));


    }

    @Override
    public void removeTrackFromFavorites(FavoriteTrackModel trackModel) {
        tracksDao.deleteTrack(trackModel);
    }
}
