package com.example.mproj3.Fragments;

import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.mproj3.Activities.LoggingActivity;
import com.example.mproj3.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

public class PasswordFragment extends Fragment {

    private TextInputLayout mailToReset;
    private AppCompatButton buttonSubmitMail;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       return inflater.inflate(R.layout.fragment_password, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mailToReset = view.findViewById(R.id.mail_to_reset_password);
        buttonSubmitMail = view.findViewById(R.id.btn_check_password);
        listeners();
    }

    private void listeners() {
        buttonSubmitMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateEmail()){
                    String mail = mailToReset.getEditText().getText().toString().trim();
                    FirebaseAuth.getInstance().sendPasswordResetEmail(mail)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Toast.makeText(getActivity(), "The reset link has been sent to your email.", Toast.LENGTH_LONG).show();
                                    Navigation.findNavController(requireActivity(), R.id.nav_host_fragment_container)
                                            .navigate(R.id.action_password_fragment_to_login_fragment);
//                                    requireActivity().getSupportFragmentManager().popBackStack();
                                }
                            });
                }
            }
        });
        
    }

    private boolean validateEmail() {

            if(!Patterns.EMAIL_ADDRESS.matcher(mailToReset.getEditText().getText().toString().trim()).matches()) {
                mailToReset.setError("Not a valid Email.");
                return false;
            }
            return true;
    }

}
