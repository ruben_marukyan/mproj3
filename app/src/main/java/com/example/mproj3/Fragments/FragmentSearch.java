package com.example.mproj3.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mproj3.Adapters.SearchedSongsAdapter;
import com.example.mproj3.Constants;
import com.example.mproj3.ModelsDeezer.DeezerSearchResponse;
import com.example.mproj3.ModelsDeezer.Track.DeezerTrack;
import com.example.mproj3.Network.Deezer.ClientDeezer;
import com.example.mproj3.Network.Deezer.DeezerJsonApi;

import com.example.mproj3.R;
import com.example.mproj3.ViewModels.SearchFragmentViewModel;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentSearch extends Fragment implements SearchedSongsAdapter.SearchedTrackListener {

    private RecyclerView recyclerView;
    private SearchedSongsAdapter adapter;
    private SearchView searchView;

    private DeezerJsonApi deezerJsonApi;
    private FragmentSearchListener callback;

    private SearchFragmentViewModel viewModel;

    public interface FragmentSearchListener{
        void onSearchedTrackClicked(int songId);
        void onClickedForFavorites(DeezerTrack deezerTrack);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentSearchListener){
            callback = (FragmentSearchListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        viewModel = new ViewModelProvider(this).get(SearchFragmentViewModel.class);
        searchView = view.findViewById(R.id.input_search);
        recyclerView = view.findViewById(R.id.recycler_search);
        listeners();
    }

    private void listeners() {
        viewModel.getTracks().observe(getViewLifecycleOwner(), new Observer<List<DeezerTrack>>() {
            @Override
            public void onChanged(List<DeezerTrack> deezerTracks) {
                if (deezerTracks!=null){
                    setRecycler(deezerTracks);
                    adapter.notifyDataSetChanged();
                }
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!searchView.getQuery().toString().equals("")){
                    doSearch(searchView.getQuery().toString().trim());
                    searchView.clearFocus();
                    return true;
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    private void doSearch(String text) {
        connectNetwork();
        searchSongs(text);
    }

    private void searchSongs(String artist) {

        Call<DeezerSearchResponse> call = deezerJsonApi.getSearchedTracks(artist, Constants.KEY_2);
        call.enqueue(new Callback<DeezerSearchResponse>() {
            @Override
            public void onResponse(Call<DeezerSearchResponse> call, Response<DeezerSearchResponse> response) {
                DeezerSearchResponse deezerSearchResponse = response.body();
                setRecycler(deezerSearchResponse.getDeezerTracks());
                viewModel.setTracks(deezerSearchResponse.getDeezerTracks());
            }

            @Override
            public void onFailure(Call<DeezerSearchResponse> call, Throwable t) {
                Toast.makeText(getActivity(), String.valueOf(t.getMessage()), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setRecycler(List<DeezerTrack> deezerTrackList) {
        adapter = new SearchedSongsAdapter(deezerTrackList,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }


    private void connectNetwork() {
        deezerJsonApi = ClientDeezer.clientInstance().create(DeezerJsonApi.class);
    }

    @Override
    public void searchedTrackClicked(int songId) {
        callback.onSearchedTrackClicked(songId);
    }

    @Override
    public void onFavoriteClicked(DeezerTrack deezerTrack) {
        callback.onClickedForFavorites(deezerTrack);
    }
//
//    @Override
//    public boolean onContextItemSelected(@NonNull MenuItem item) {
//
//        if (item.getItemId()==303){
//
//            Toast.makeText(getActivity(),  "" +  Objects.requireNonNull(viewModel.getTracks().getValue()).get(item.getGroupId()).getTitle(), Toast.LENGTH_SHORT ).show();
//            //ToDo add in firebase and Room
//        }
////        return super.onContextItemSelected(item);
//        return true;
//    }
}
