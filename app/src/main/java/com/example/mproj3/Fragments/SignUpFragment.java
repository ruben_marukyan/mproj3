package com.example.mproj3.Fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.example.mproj3.Dialogues.MyDatePickerDialog;
import com.example.mproj3.Models.UserModel;
import com.example.mproj3.R;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class SignUpFragment extends Fragment implements DatePickerDialog.OnDateSetListener{

    private TextInputLayout mail;
    private TextInputLayout username;
    private TextInputLayout password;
    private TextInputLayout passwordConfirm;
    private TextInputLayout phoneNumber;
    private AppCompatTextView textBirthday;
    private AppCompatImageView iconBirthday;
    private AppCompatImageView iconError;

    private AppCompatButton buttonSignUp;

    private SignUpFragmentListener callback;

    private boolean birthdayPicked = false;

    private UserModel user;

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        String birthday = dayOfMonth + "/" + month + "/" + year;
        textBirthday.setText(birthday);
        birthdayPicked = true;
    }

    public interface SignUpFragmentListener{
        void signUpWithNewUser(UserModel user, String password);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof SignUpFragmentListener){
            callback = (SignUpFragmentListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mail = view.findViewById(R.id.txt_input_mail);
        username = view.findViewById(R.id.txt_input_username);
        password = view.findViewById(R.id.txt_input_password);
        passwordConfirm = view.findViewById(R.id.txt_input_password_confirm);
        phoneNumber = view.findViewById(R.id.txt_input_phone_number);
        textBirthday = view.findViewById(R.id.txt_picked_date);
        buttonSignUp = view.findViewById(R.id.btn_sign_up);
        iconBirthday = view.findViewById(R.id.img_birthday);
        iconError = view.findViewById(R.id.error_datePick);

        listeners();
    }

    private void listeners() {

        iconBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBirthdayPickDialog();
            }
        });
        textBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBirthdayPickDialog();
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidations()){
                    callback.signUpWithNewUser(new UserModel(
                            mail.getEditText().getText().toString(),
                            username.getEditText().getText().toString(),
                            phoneNumber.getEditText().getText().toString(),
                            textBirthday.getText().toString()
                    ), password.getEditText().getText().toString());
                }
            }
        });
    }

    private boolean checkValidations(){
        return validateEmail() && validateUsername()
                && validatePasswords() && validatePhoneNumber()
                && birthdayPicked();
    }

    private boolean birthdayPicked() {
        if (!birthdayPicked){
            iconError.setVisibility(View.VISIBLE);
        }
        else {
            iconError.setVisibility(View.GONE);
        }
        return birthdayPicked;
    }

    private boolean validatePhoneNumber() {
        phoneNumber.setError(null);

        if(phoneNumber.getEditText().getText().toString().equals("")){
            phoneNumber.setError("Add your phone number");
            return false;
        }

        return true;
    }

    private void openBirthdayPickDialog() {

        Calendar calendar =Calendar.getInstance();
        int year=calendar.get(Calendar.YEAR);
        int month=calendar.get(Calendar.MONTH);
        int day=calendar.get(Calendar.DAY_OF_MONTH);

        new MyDatePickerDialog(requireActivity(), this, year, month, day)
                .show();
//        DatePickerDialog datePickerDialog = new DatePickerDialog(requireActivity(), this,year, month, day);
//        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
//        datePickerDialog.show();

    }
    private boolean validatePasswords() {

        password.setError(null);
        passwordConfirm.setError(null);
        if (Objects.requireNonNull(password.getEditText()).getText().toString().equals("")){
            password.setError("Write your password.");
            return false;
        }else if(password.getEditText().getText().length() < 6){
            password.setError("Password should be at least 6 characters!");

        }else if(!password.getEditText().getText().toString().equals(passwordConfirm.getEditText().getText().toString())){
            password.setError("Passwords didn't match!");
            passwordConfirm.setError("Passwords didn't match!");
            return false;
        } else if (password.getEditText().getText().toString().equals(passwordConfirm.getEditText().getText().toString())){
            password.setEndIconDrawable(R.drawable.ic_check);
            passwordConfirm.setEndIconDrawable(R.drawable.ic_check);
            return true;
        }

        return true;
    }

    private boolean validateUsername() {

        username.setError(null);

        if(username.getEditText().getText().toString().equals("")){
            username.setError("Write your username!");
            return false;
        }
        return true;
    }

    private boolean validateEmail() {
        mail.setError(null);
        if(mail.getEditText().getText().toString().equals("")){
            mail.setError("Write your Email.");
            return false;
        }  else if(!Patterns.EMAIL_ADDRESS.matcher(mail.getEditText().getText().toString().trim()).matches()){
            mail.setError("Not a valid Email.");
            return false;
        }
        return true;
    }
}
