package com.example.mproj3.Fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.mproj3.Activities.LoggingActivity;
import com.example.mproj3.Activities.MainActivity;
import com.example.mproj3.Constants;
import com.example.mproj3.Database.FavoriteTracksRoomDB;
import com.example.mproj3.Dialogues.MyDatePickerDialog;
import com.example.mproj3.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;

public class FragmentProfile extends Fragment implements DatePickerDialog.OnDateSetListener {

    private AppCompatTextView profile_mail;
    private AppCompatTextView profile_username;
    private AppCompatTextView profile_birthday;
    private AppCompatTextView profile_phone_number;
    private AppCompatTextView profile_favorite_count;
    private FloatingActionButton floatingActionButton;

    private AppCompatEditText edit_username;
    private AppCompatEditText edit_phone_number;
    private AppCompatImageView edit_date;

    private String username;
    private String phoneNumber;

    private boolean editable = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        profile_mail = view.findViewById(R.id.profile_mail);
        profile_username = view.findViewById(R.id.profile_username);
        profile_birthday = view.findViewById(R.id.profile_birthday);
        profile_phone_number = view.findViewById(R.id.profile_phone);
        profile_favorite_count = view.findViewById(R.id.profile_favorites_count);
        floatingActionButton = view.findViewById(R.id.floating_action_button);

        edit_username = view.findViewById(R.id.profile_editable_username);
        edit_phone_number = view.findViewById(R.id.profile_editable_phone);
        edit_date = view.findViewById(R.id.date_edit);

        listeners();
        setUi();
    }

    private void setUi() {
         String userId = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        DocumentReference documentReference = firebaseFirestore
                .collection(Constants.USERS_DOCUMENT_COLLECTION)
                .document(userId);
        documentReference.addSnapshotListener(requireActivity(), new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                if (value != null) {
                    Map<String, Object> document = value.getData();

                    assert document != null;
                    String mail = Objects.requireNonNull(document.get(Constants.USER_MAIL)).toString();
                    username = Objects.requireNonNull(document.get(Constants.USER_USERNAME)).toString();
                    phoneNumber = Objects.requireNonNull(document.get(Constants.USER_PHONE_NUMBER)).toString();
                    String birthday = Objects.requireNonNull(document.get(Constants.USER_BIRTHDAY)).toString();
                    int count = FavoriteTracksRoomDB.getInstance(getActivity()).favoriteTracksDao().getFavoriteTracks(userId).size();
                    String favorite;

                    if (count == 0) {
                        favorite = "No favorite tracks";
                    } else {
                        favorite = count + " favorite tracks";
                    }
                    profile_mail.setText(mail);
                    profile_username.setText(username);
                    profile_phone_number.setText(phoneNumber);
                    profile_birthday.setText(birthday);
                    profile_favorite_count.setText(favorite);

                }
            }
        });
    }

    private void listeners() {

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editable){
                    editable = true;
                    floatingActionButton.setImageResource(R.drawable.ic_check);

                } else {
                    editable = false;
                    floatingActionButton.setImageResource(R.drawable.ic_edit);
                }
                changeUi(editable);
            }
        });
        edit_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBirthdayPickDialog();
            }
        });

    }

    private void openBirthdayPickDialog() {

        Calendar calendar =Calendar.getInstance();
        int year=calendar.get(Calendar.YEAR);
        int month=calendar.get(Calendar.MONTH);
        int day=calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(requireActivity(), this,year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();

    }

    private void changeUi(boolean editable) {
        if (editable){
            profile_username.setVisibility(View.GONE);
            profile_phone_number.setVisibility(View.GONE);
            edit_date.setVisibility(View.VISIBLE);
            edit_phone_number.setVisibility(View.VISIBLE);
            edit_phone_number.setText(phoneNumber);
            edit_username.setVisibility(View.VISIBLE);
            edit_username.setText(username);
        }
        else {
            profile_username.setText(edit_username.getText());
            profile_phone_number.setText(edit_phone_number.getText());
            profile_username.setVisibility(View.VISIBLE);
            profile_phone_number.setVisibility(View.VISIBLE);
            edit_date.setVisibility(View.GONE);
            edit_phone_number.setVisibility(View.GONE);
            edit_username.setVisibility(View.GONE);

            openDocumentSnapshot();
        }
    }

    private void openDocumentSnapshot() {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DocumentReference documentReference = FirebaseFirestore.getInstance()
                .collection(Constants.USERS_DOCUMENT_COLLECTION)
                .document(userId);
        Map<String, String> userFirebase = new HashMap<>();
        userFirebase.put(Constants.USER_MAIL, profile_mail.getText().toString());
        userFirebase.put(Constants.USER_USERNAME, profile_username.getText().toString());
        userFirebase.put(Constants.USER_PHONE_NUMBER, profile_phone_number.getText().toString());
        userFirebase.put(Constants.USER_BIRTHDAY, profile_birthday.getText().toString());
        documentReference.set(userFirebase).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(), "User updated", Toast.LENGTH_SHORT).show();
                Log.d("TAG", "onSuccess: user with ID  " + userId);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getActivity(), "Some error happened", Toast.LENGTH_SHORT).show();
                Log.d("TAG", "onFailure: " + e.toString());
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String currentDate = DateFormat.getDateInstance().format(calendar.getTime());
        profile_birthday.setText(currentDate);
    }
}
