
package com.example.mproj3.ModelsDeezer.Track;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeezerTrack {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("readable")
    @Expose
    private Boolean readable;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("title_short")
    @Expose
    private String titleShort;
    @SerializedName("title_version")
    @Expose
    private String titleVersion;
    @SerializedName("isrc")
    @Expose
    private String isrc;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("share")
    @Expose
    private String share;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("track_position")
    @Expose
    private Integer trackPosition;
    @SerializedName("disk_number")
    @Expose
    private Integer diskNumber;
    @SerializedName("rank")
    @Expose
    private Integer rank;
    @SerializedName("release_date")
    @Expose
    private String releaseDate;
    @SerializedName("explicit_lyrics")
    @Expose
    private Boolean explicitLyrics;
    @SerializedName("explicit_content_lyrics")
    @Expose
    private Integer explicitContentLyrics;
    @SerializedName("explicit_content_cover")
    @Expose
    private Integer explicitContentCover;
    @SerializedName("preview")
    @Expose
    private String preview;
    @SerializedName("bpm")
    @Expose
    private Float bpm;
    @SerializedName("gain")
    @Expose
    private Float gain;
    @SerializedName("available_countries")
    @Expose
    private List<String> availableCountries = null;
    @SerializedName("contributors")
    @Expose
    private List<Contributor> contributors = null;
    @SerializedName("md5_image")
    @Expose
    private String md5Image;
    @SerializedName("artist")
    @Expose
    private Artist artist;
    @SerializedName("album")
    @Expose
    private Album album;
    @SerializedName("type")
    @Expose
    private String type;

    public DeezerTrack() {
    }

    /**
     * 
     * @param preview
     * @param trackPosition
     * @param artist
     * @param link
     * @param md5Image
     * @param title
     * @param type
     * @param gain
     * @param duration
     * @param titleVersion
     * @param rank
     * @param titleShort
     * @param share
     * @param id
     * @param explicitContentCover
     * @param bpm
     * @param readable
     * @param releaseDate
     * @param album
     * @param diskNumber
     * @param explicitContentLyrics
     * @param isrc
     * @param explicitLyrics
     * @param contributors
     * @param availableCountries
     */
    public DeezerTrack(Integer id, Boolean readable, String title, String titleShort, String titleVersion, String isrc, String link, String share, Integer duration, Integer trackPosition, Integer diskNumber, Integer rank, String releaseDate, Boolean explicitLyrics, Integer explicitContentLyrics, Integer explicitContentCover, String preview, Float bpm, Float gain, List<String> availableCountries, List<Contributor> contributors, String md5Image, Artist artist, Album album, String type) {
        super();
        this.id = id;
        this.readable = readable;
        this.title = title;
        this.titleShort = titleShort;
        this.titleVersion = titleVersion;
        this.isrc = isrc;
        this.link = link;
        this.share = share;
        this.duration = duration;
        this.trackPosition = trackPosition;
        this.diskNumber = diskNumber;
        this.rank = rank;
        this.releaseDate = releaseDate;
        this.explicitLyrics = explicitLyrics;
        this.explicitContentLyrics = explicitContentLyrics;
        this.explicitContentCover = explicitContentCover;
        this.preview = preview;
        this.bpm = bpm;
        this.gain = gain;
        this.availableCountries = availableCountries;
        this.contributors = contributors;
        this.md5Image = md5Image;
        this.artist = artist;
        this.album = album;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getReadable() {
        return readable;
    }

    public void setReadable(Boolean readable) {
        this.readable = readable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleShort() {
        return titleShort;
    }

    public void setTitleShort(String titleShort) {
        this.titleShort = titleShort;
    }

    public String getTitleVersion() {
        return titleVersion;
    }

    public void setTitleVersion(String titleVersion) {
        this.titleVersion = titleVersion;
    }

    public String getIsrc() {
        return isrc;
    }

    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getTrackPosition() {
        return trackPosition;
    }

    public void setTrackPosition(Integer trackPosition) {
        this.trackPosition = trackPosition;
    }

    public Integer getDiskNumber() {
        return diskNumber;
    }

    public void setDiskNumber(Integer diskNumber) {
        this.diskNumber = diskNumber;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Boolean getExplicitLyrics() {
        return explicitLyrics;
    }

    public void setExplicitLyrics(Boolean explicitLyrics) {
        this.explicitLyrics = explicitLyrics;
    }

    public Integer getExplicitContentLyrics() {
        return explicitContentLyrics;
    }

    public void setExplicitContentLyrics(Integer explicitContentLyrics) {
        this.explicitContentLyrics = explicitContentLyrics;
    }

    public Integer getExplicitContentCover() {
        return explicitContentCover;
    }

    public void setExplicitContentCover(Integer explicitContentCover) {
        this.explicitContentCover = explicitContentCover;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public Float getBpm() {
        return bpm;
    }

    public void setBpm(Float bpm) {
        this.bpm = bpm;
    }

    public Float getGain() {
        return gain;
    }

    public void setGain(Float gain) {
        this.gain = gain;
    }

    public List<String> getAvailableCountries() {
        return availableCountries;
    }

    public void setAvailableCountries(List<String> availableCountries) {
        this.availableCountries = availableCountries;
    }

    public List<Contributor> getContributors() {
        return contributors;
    }

    public void setContributors(List<Contributor> contributors) {
        this.contributors = contributors;
    }

    public String getMd5Image() {
        return md5Image;
    }

    public void setMd5Image(String md5Image) {
        this.md5Image = md5Image;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
