package com.example.mproj3.ModelsDeezer;

import java.util.List;

import com.example.mproj3.ModelsDeezer.Track.DeezerTrack;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeezerSearchResponse {

    @SerializedName("data")
    @Expose
    private List<DeezerTrack> deezerTracks = null;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("prev")
    @Expose
    private String prev;
    @SerializedName("next")
    @Expose
    private String next;
    
    public DeezerSearchResponse() {
    }

    /**
     * 
     * @param next
     * @param total
     * @param data
     * @param prev
     */
    public DeezerSearchResponse(List<DeezerTrack> data, Integer total, String prev, String next) {
        super();
        this.deezerTracks = data;
        this.total = total;
        this.prev = prev;
        this.next = next;
    }

    public List<DeezerTrack> getDeezerTracks() {
        return deezerTracks;
    }

    public void setDeezerTracks(List<DeezerTrack> deezerTracks) {
        this.deezerTracks = deezerTracks;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getPrev() {
        return prev;
    }

    public void setPrev(String prev) {
        this.prev = prev;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

}
